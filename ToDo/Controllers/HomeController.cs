﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDo.Entities;
using ToDo.Models;

namespace ToDo.Controllers
{
    public class HomeController : Controller
    {
        private Context context=new Context();

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Index() {
            return View();
        }

        public IActionResult LoginAction(UserLoginViewModel model) {
            var usr = context.Users.FirstOrDefault(x => x.Name == model.Name && x.Password == model.Password);
            if (usr != null)
            {
                var cookieOptions = new Microsoft.AspNetCore.Http.CookieOptions()
                {
                    Path = "/",
                    HttpOnly = false,
                    IsEssential = true, //<- there
                    Expires = DateTime.Now.AddMonths(1),
                };
                HttpContext.Response.Cookies.Append("UserId", usr.Id.ToString(),cookieOptions);
                return View("TodoIndex", context.Todos.Where(x => x.CreatorId == usr.Id).ToList());
            }
            else {
                return View("Login",model);
            }
            
        }
        public IActionResult SingIn(UserCreateViewModel model) { 
            return View();
        }
        public IActionResult SingInAction(UserCreateViewModel model)
        {
            context = new Context();
            var usr = context.Users.FirstOrDefault(x => x.Name == model.Name && x.Password == model.Password);
            if (!ModelState.IsValid || usr!=null)
            {
                return View("SingIn", model);
            }
            User usrr = new User();
            usrr.Name = model.Name;
            usrr.Password = model.Password;
            context.Users.Add(usrr);
            context.SaveChanges();
            Response.Cookies.Append("UserId", usrr.Id.ToString());
            return RedirectToAction("TodoIndex");
        }

        #region TODO
        public IActionResult TodoIndex() {
            string textCookie = Request.Cookies["UserId"];
            if (textCookie == null) {
                return RedirectToAction("Login");
            }
            int cookie=int.Parse(textCookie);
            return View(context.Todos.Where(x=>x.CreatorId==cookie).ToList());
        }
        public ActionResult TodoCreate()
        {
            return View();
        }

        public IActionResult TodoEdit(int id)
        {
            var act = context.Todos.Where(y => y.Id == id).FirstOrDefault();
            if (act == null)
            {
                return View("TodoIndex");
            }
            return View(new Models.TodoViewModel() { Id=id,Name=act.Name,Description=act.Description,Month=act.Time.Month,Day=act.Time.Day,Hour=act.Time.Hour,CreatorId=act.CreatorId });

        }

        public IActionResult TodoDelete(int id)
        {
            var act = context.Todos.Where(y => y.Id == id).FirstOrDefault();
            if (act != null)
            {
                context.Entry(act).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                context.SaveChanges();
            }
            return RedirectToAction("TodoIndex");
        }

        [HttpPost]
        public ActionResult TodoEdit(Models.TodoViewModel act)
        {
            if (!ModelState.IsValid)
            {
                return View(act);
            }

            var temp=context.Todos.FirstOrDefault(x=>x.Id==act.Id);
            temp.Name = act.Name;
            temp.Description = act.Description;
            temp.Time = act.getDate();

            context.Entry(temp).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();

            return RedirectToAction("TodoIndex");
        }

        [HttpPost]
        public ActionResult TodoCreate(Models.TodoViewModel act)
        {
            if (!ModelState.IsValid)
            {
                return View(act);
            }
            act.Id=int.Parse(Request.Cookies["UserId"].ToString());
            context.Todos.Add(new Todo() { Name = act.Name, Description = act.Description, Time = act.getDate(), CreatorId = int.Parse(Request.Cookies["UserId"].ToString()),Done=false });
            context.SaveChanges();

            return RedirectToAction("TodoIndex");
        }
        #endregion

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
