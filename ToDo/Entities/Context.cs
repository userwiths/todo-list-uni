﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Models;

namespace ToDo.Entities
{
    public class Context : DbContext
    {
        public Context() : base()
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Todo> Todos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer("Persist Security Info=False;Integrated Security=true;server=localhost\\SQLEXPRESS;Initial Catalog=ToDo;");
        protected override void OnModelCreating(ModelBuilder builder) {
            builder.Entity<Todo>().HasOne(t => t.Creator).WithMany(u => u.ToDoList);
        }
        public DbSet<ToDo.Models.UserLoginViewModel> UserLoginViewModel { get; set; }
        public DbSet<ToDo.Models.UserCreateViewModel> UserCreateViewModel { get; set; }
    }
}
