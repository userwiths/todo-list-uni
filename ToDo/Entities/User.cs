﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo.Entities
{
    [Table("Users")]
    public class User:Base
    {
        public string Password { get; set; }

        public virtual ICollection<Todo> ToDoList { get; set; }
    }
}
