﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo.Entities
{
    [Table("Todos")]
    public class Todo:Base
    {
        public bool Done { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }

        public int CreatorId { get; set; }
        public User Creator { get; set; }
    }
}
