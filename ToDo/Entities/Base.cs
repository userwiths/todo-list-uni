﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo.Entities
{
    public class Base
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Base() { }
        public Base(string name) {
            this.Name = name;
        }
    }
}
