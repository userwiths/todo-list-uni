﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo.Models
{
    public class UserLoginViewModel
    {
        //[Id]
        public int Id { get; set; }

        [Required]
        public string Name { get; set;}

        [Required]
        public string Password { get; set; }
    }
}
