﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo.Models
{
    public class TodoViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Hour { get; set; }
        public int CreatorId { get; set; }

        public DateTime getDate() {
            var temp = DateTime.Now;
            return new DateTime(temp.Year, this.Month, this.Day, this.Hour, 0, 0);
        }
    }
}
